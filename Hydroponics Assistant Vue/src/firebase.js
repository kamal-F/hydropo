// Config file
import * as firebase from 'firebase'
//config your firebase on vue.js
let config = {
  apiKey: '',
  authDomain: '',
  databaseURL: '',
  projectId: '',
  storageBucket: '',
  messagingSenderId: ''
}

export default !firebase.apps.length ? firebase.initializeApp(config) : firebase.app()

